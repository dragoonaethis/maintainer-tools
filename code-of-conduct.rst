.. _code-of-conduct:

===============
Code of Conduct
===============

Please be aware the freedesktop.org Code of Conduct applies to projects it
hosts, including the drm subsystem, igt, maintainer tools, etc:

https://www.freedesktop.org/wiki/CodeOfConduct/

Please be aware also the kernel.org Code of Conduct applies to kernel
development:

https://www.kernel.org/code-of-conduct.html

See the kernel MAINTAINERS file for contact details of the drm maintainers.

Abuse of commit rights, like engaging in commit fights or willfully pushing
patches that violate the documented merge criteria or process, will also be
handled through the Code of Conduct enforcement process. Violations may lead to
temporary or permanent account or commit rights suspension according to
freedesktop.org umbrella rules.
