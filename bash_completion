#! /bin/bash

DIM_CACHE_DIR=${XDG_CACHE_HOME:-$HOME/.cache}/dim

dim_last_path_file=$DIM_CACHE_DIR/last-path

dim ()
{
	if [[ "$1" = "cd" ]]; then
		if [ -e "$dim_last_path_file" ]; then
			cd $(cat $dim_last_path_file) || return 1
		else
			return 1
		fi
	else
		command dim "$@"
	fi
}

_dim_git_branches()
{
	git for-each-ref --format='%(refname:short)' refs/heads
}

_dim ()
{
	local args arg cur prev words cword split
	local nightly_branches upstream_branches opts cmds aliasref

	# require bash-completion with _init_completion
	type -t _init_completion >/dev/null 2>&1 || return

	_init_completion || return

	COMPREPLY=()

	# arg = subcommand
	_get_first_arg

	# args = number of arguments
	_count_args

	nightly_branches="$(dim list-branches)"
	upstream_branches="$(dim list-upstreams)"

	if [ -z "${arg}" ]; then
		# top level completion
		case "${cur}" in
			-*)
				opts="-d -f -i"
				COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
				;;
			*)
				cmds="$(dim list-commands) $(dim list-aliases | sed 's/\t.*//')"
				COMPREPLY=( $(compgen -W "${cmds}" -- ${cur}) )
				;;
		esac
		return 0
	fi

	# complete aliases like the actual command
	aliasref=$(dim list-aliases | sed -n "s/^${arg}\t\(.*\)/\1/p")
	if [[ -n "$aliasref" ]]; then
		arg="$aliasref"
	fi

	case "${arg}" in
		push-branch)
			COMPREPLY=( $( compgen -W "-f $nightly_branches" -- $cur ) )
			;;
		push-queued|push-fixes|push-next-fixes)
			COMPREPLY=( $( compgen -W "-f" -- $cur ) )
			;;
		cat-to-fixup|apply-branch)
			COMPREPLY=( $( compgen -W "$nightly_branches" -- $cur ) )
			;;
		magic-patch)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -o nospace -W "-a" -- $cur ) )
			fi
			;;
		tc|fixes)
			# FIXME needs a git sha1
			;;
		checkpatch)
			# FIXME needs a git sha1
			;;
		pull-request|backmerge)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches" -- $cur ) )
			elif [[ $args == 3 ]]; then
				COMPREPLY=( $( compgen -W "$upstream_branches" -- $cur ) )
			elif [[ $args == 4 ]]; then
				COMPREPLY=( $( compgen -W "$(git tag --list "${words[2]}-*")" -- $cur ) )
			fi
			;;
		tag-branch)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches" -- $cur ) )
			elif [[ $args == 3 ]]; then
				COMPREPLY=( $( compgen -W "$upstream_branches" -- $cur ) )
			fi
			;;
		pull-request-next|pull-request-fixes|pull-request-next-fixes)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$upstream_branches" -- $cur ) )
			fi
			;;
		cherry-pick-branch)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches" -X "!*-fixes" -- $cur ) )
			fi
			;;
		create-branch)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -o nospace -W "drm- topic/" -- $cur ) )
			fi
			;;
		checkout)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches" -- $cur ) )
			fi
			;;
		remove-branch)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches" -- $cur ) )
			fi
			;;
		create-workdir)
			if [[ $args == 2 ]]; then
				COMPREPLY=( $( compgen -W "$nightly_branches all" -- $cur ) )
			fi
			;;
		retip)
			COMPREPLY=($(compgen -W "$(_dim_git_branches)" -- $cur))
			;;
	esac

	return 0
}
complete -F _dim dim
