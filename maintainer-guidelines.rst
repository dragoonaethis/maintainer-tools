.. _maintainer-guidelines:

=====================
Maintainer Guidelines
=====================

This document gathers together maintainer guidelines.

.. toctree::
   :maxdepth: 2

   maintainer-drm-misc
   maintainer-drm-intel
   maintainer-drm-xe
